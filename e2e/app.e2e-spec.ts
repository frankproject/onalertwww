import { OnalertPage } from './app.po';

describe('onalert App', function() {
  let page: OnalertPage;

  beforeEach(() => {
    page = new OnalertPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
