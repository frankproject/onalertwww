import { Injectable } from '@angular/core';
import { Http , Response, Headers, URLSearchParams  } from '@angular/http';

import { environment } from '../../environments/environment';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class RequestService {
  private api_url = environment.api_url;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  });


  constructor(public http: Http) {}

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getRegulations(folio: string): Promise<any> {

    let params: URLSearchParams = new URLSearchParams();
    params.append('folio', folio );
    
    return this.http
            .get(`${this.api_url}/api/regulations`, {headers: this.headers, search: params} )
            .toPromise()
            .then(response => response.json().requests)
            .catch(this.handleError);

  }

  getRegulation(case_id: string): Promise<any> {

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', case_id );
    
    return this.http
            .get(`${this.api_url}/api/regulation`, {headers: this.headers, search: params} )
            .toPromise()
            .then(response => response.json().requests)
            .catch(this.handleError);

  }

  getRegulationTables(case_id: string): Promise<any> {

    let params: URLSearchParams = new URLSearchParams();
    params.append('id', case_id );
    
    return this.http
            .get(`${this.api_url}/api/regulations-table`, {headers: this.headers, search: params} )
            .toPromise()
            .then(response => response.json().requests)
            .catch(this.handleError);

  }

}