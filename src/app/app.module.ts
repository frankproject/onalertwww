import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { AgmCoreModule } from "angular2-google-maps/core";

import { environment } from "../environments/environment";

import { routing } from './app.routing';
import { MapComponent } from './map/map.component';
import { HomeComponent } from './home/home.component';

import { MapService } from './services/map.services';
import { RequestService } from './services/request.services';

import { AlertModule, ModalModule } from 'ngx-bootstrap';

import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    HomeComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: environment.google_api_key,
      libraries: ["places"]
    }),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    Ng2SmartTableModule
  ],
  providers: [
    MapService,
    RequestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
