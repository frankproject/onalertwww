import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

//import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
//import { MapComponent } from './map/map.component';
import { HomeComponent } from './home/home.component';

export const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
    }/*,
    {
        path: '**',
        component: PageNotFoundComponent
    }*/

];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);