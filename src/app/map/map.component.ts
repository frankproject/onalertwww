import { Component, NgZone, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from "@angular/forms";
import { MapsAPILoader } from 'angular2-google-maps/core';
import { LocalDataSource } from 'ng2-smart-table';

import { MapService } from '../services/map.services';
import { RequestService } from '../services/request.services';

import { ModalDirective } from 'ng2-bootstrap';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  private folio: string;
  public zoom: number;

  private street;
  private address;
  private city;
  private state;
  private country;
  private zipcode;

  private show_table: boolean = false;

  @ViewChild("search")
  public searchElementRef: ElementRef;
  @ViewChild('infoModal') infoModal: ModalDirective;
  @ViewChild('infoTablesModal') infoTablesModal: ModalDirective;

  private regulation_info: any[] = [];
  private regulation_info_tables: any[] = [];

  settings = {
    mode: 'external',
    actions: {
      add: false,
      delete: false,
      edit: {
        editButtonContent: 'View'
      }
    },
    pager: {
      perPage: 10
    },
    columns: {
      id: {
        title: 'ID'
      },
      type: {
        title: 'Type'
      },
      address: {
        title: 'Address'
      },
      owner: {
        title: 'Owner'
      },
      folio: {
        title: 'Folio'
      }
    }
  };

  source: LocalDataSource;

  constructor(
      private mapsAPILoader: MapsAPILoader,
      private ngZone: NgZone,
      private mapService: MapService,
      private requestService: RequestService
  ) { 
    this.source = new LocalDataSource();
  }

  ngOnInit() {
    //set google maps defaults
    this.zoom = 18;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    let componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //console.log("Place: " + JSON.stringify(place));

          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              
              if (addressType == "street_number") {
                  this.street = val;
              }

              if (addressType == "route") {
                  this.address = val;
              }

              if (addressType == "locality") {
                  this.city = val;
              }

              if (addressType == "administrative_area_level_1") {
                  this.state = val;
              }

              if (addressType == "country") {
                  this.country = val;
              }

              if (addressType == "postal_code") {
                  this.zipcode = val;
              }
            }
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 18;
        });
      });
    });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 18;
      });
    }
  }

  sendRequest(): void {
    this.requestService.getRegulations(this.folio).then( data =>{
        if ( data.length > 0 )
          this.show_table = true;
        this.source.load(data);
    });
  }

  onUserRowSelect(event): void {
      this.requestService.getRegulation(event.data.id).then( data =>{
        this.regulation_info = data;
        this.infoModal.show();
      });
      
    }

  edit (event): void {
    this.requestService.getRegulationTables(event.data.id).then( data =>{
        this.regulation_info_tables = data;
        this.infoTablesModal.show();
      });
  }
}
